package cf.pasa;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws UnsupportedEncodingException {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Query : ");
        String query = scanner.nextLine();
        InstantAnswer ia = new InstantAnswer(query);
        ia.call();

        System.out.println("Waiting response...");
    }
}
