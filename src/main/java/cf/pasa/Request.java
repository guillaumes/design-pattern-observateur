package cf.pasa;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Observable;

/**
 * Request is an Observable making async GET
 *
 * @author Samuel Guillaume
 * @see java.util.Observable
 */
public class Request extends Observable implements Runnable {

    private final String USER_AGENT = "Mozilla/5.0";

    private Thread thread;
    private String url;

    /**
     * Start the request
     * @param url request target
     */
    public Request(String url) {
        this.url = url;
        if (this.thread == null) {
            this.thread = new Thread(this);
            this.thread.start ();
        }
    }

    /**
     * Makes the request async
     * @see java.lang.Runnable
     */
    public void run() {
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();

            if (responseCode == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // Notifies observers
                this.setChanged();
                this.notifyObservers(response.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
