package cf.pasa;

import com.google.gson.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Observable;
import java.util.Observer;

/**
 * Use the DuckDuckGo API to give answer to query (https://duckduckgo.com/api)
 * @see java.util.Observer
 */
public class InstantAnswer implements Observer {

    private String url;

    /**
     * Create an InstantAnswer instance and validate the query
     * @param query to search on DuckDuckGo
     * @throws UnsupportedEncodingException if the query isn't valid
     */
    public InstantAnswer(String query) throws UnsupportedEncodingException {
        this.url = "http://api.duckduckgo.com/?format=json&q=" + URLEncoder.encode(query, "UTF-8");
    }

    /**
     * Make the call to the DuckDuckGo API
     */
    public void call() {
        Request request = new Request(url);
        request.addObserver(this);
    }

    /**
     * @see java.util.Observer
     */
    public void update(Observable observable, Object obj)
    {
        if (observable instanceof Request) {
            String response = (String) obj;
            this.printResponse(response);
            observable.deleteObservers();
        }
    }

    /**
     * Parse and print in the console the answer to the query
     * @param response json from DuckDuckGo
     */
    private void printResponse(String response) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(response).getAsJsonObject();

        String definition = json.get("Abstract").getAsString();

        if (definition.length() > 0) {
            System.out.println(definition);
            System.out.println("Source : " + json.get("AbstractSource").getAsString());
            return;
        } else {
            JsonArray relatedTopics = json.get("RelatedTopics").getAsJsonArray();

            if (relatedTopics.size() > 0) {
                JsonObject related = relatedTopics.get(0).getAsJsonObject();

                String text = null;
                String source = "";

                JsonElement textElement = related.get("Text");
                if (textElement != null) {
                    text = textElement.getAsString();
                    source = related.get("FirstURL").getAsString();
                } else {
                    JsonElement topicsElements =  related.get("Topics");
                    if (topicsElements != null) {
                        JsonArray topicsArray = topicsElements.getAsJsonArray();
                        if (topicsArray.size() > 0) {
                            JsonObject topic = topicsArray.get(0).getAsJsonObject();
                            text = topic.get("Text").getAsString();
                            source = topic.get("FirstURL").getAsString();
                        }
                    }
                }



                if (text != null && text.length() > 0) {
                    System.out.println(text);
                    System.out.println("Source : " + source);
                    return;
                }
            }
        }
        System.out.println("Sorry, I found nothing.");
    }
}
